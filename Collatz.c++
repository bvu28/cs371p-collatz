// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <map>		// map for cache
#include "Collatz.h"

using namespace std;
int* cache = new int[999999];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>
    if(i > j)								//swaps arguments if the first one is greater
        return collatz_eval(j, i);

    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);

    int condition = (j/2) + 1;				//optimization from class
    if(i < condition)
        return collatz_eval(condition, j);

    int max = 1;

    for (int n = i; n < j+1; ++n)
    {
        int mapped = n;
        uint32_t num = n;
        int cycle = 1;
        while (num != 1)
        {
            if (cache[mapped] != 0)
            {
                cycle += cache[num] - 1;
                break;
            }
            if (num % 2 == 0)
            {
                num/=2;
                ++cycle;
            }
            else
            {
                num = num + (num/2) + 1;
                cycle += 2;
            }
        }
        cache[mapped] = cycle;
        if (cycle > max)
            max = cycle;
    }
    return max;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
